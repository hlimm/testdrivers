"""
Wrapper for the Teledyne-LeCroy HDO4000 oscilloscope for scripting features
such as reading out analog channels and using the CAN software package. This
uses LeCroy's ActiveDSO driver for communicating (currently over USBTMC),
rather than VISA. If this becomes an issue, make sure there are no hardware
address collisions, and if that doesn't work, consider switching to using a
TCPIP connection.

Adapted from:
https://teledynelecroy.com/doc/using-python-with-activedso-for-remote-communication

Written on Feb 18 2021
@author: Henry Limm
Last Update: 
"""

'''
NOTE:
This class will need a few programs installed before it is useable:
    ActiveDSO from TeledyneLeCroy (https://teledynelecroy.com/support/softwaredownload/activedso.aspx)
    NI-VISA from National Instruments (https://www.ni.com/en-us/support/downloads/drivers/download.ni-visa.html#346210)

I'm not too clear as to how these are 'used' when connecting to the scope, 
but from what I understand, ActiveDSO is necessary for creating a 
win32com.client object that can interface with the scope, and NI-VISA is 
somehow necessary for the USBTMC layer of the connection.

Also of note is that the oscilloscope may not appear as a USBTMC device right
away. I recommend using the Device Manager (right click the Start button > 
Device Manager, or just search the phrase) to see if the scope is connected.
It may need to be plugged and unplugged a few times. Be sure that you're 
connecting to the scope with its USB Type B port, not any of the Type A ports.
'''

import win32com.client as com_cl # import from the pywin32 library

# NOTE: need to have ActiveDSO installed on the controlling (Windows) computer

class HDOScope:
    '''
    This is the wrapper for controlling the oscilloscope over with ActiveDSO.
    Hopefully this will be able to do everything that we need it to without
    much screwing around.
    TODO: add descriptions of each function available
    Attributes:
    _addr : str
        Address of the oscilloscope
    _protocol : str
        Protocol through which the scope is controlled (USBTMC, TCP/IP, etc.)
    '''
    
    def __init__(self, addr, protocol='USBTMC'):
        '''
        Create an instance of the LeCroy HDO oscilloscope using ActiveDSO.

        Arguments: 
        addr : str
            address of the oscilloscope
        protocol : str
            Communication protocol for controlling the scope (USBTMC, TCP/IP)
        '''
        self. _addr = addr
        self._protocol = protocol
        # NOTE: it may not be necessary to make the above attributes, so they're protected
        self.scope = com_cl.Dispatch('LeCroy.ActiveDSOCtrl.1') # start ActiveX control of ActiveDSO
        connected = self.scope.MakeConnection(protocol + ':' + addr) # establish connection to the scope
        if not connected:
            print('Could not connect to the scope. Test will exit.')
            sys.exit()
        else:
            print('Connected to the scope at: ' + addr)

    def get_scaled_waveform(self, channel, maxbytes, array):
        ''' Wrapper for the GetScaledWaveform ActiveDSO Method '''
        self.scope.GetScaledWaveform(channel, maxbytes, whichArray)

