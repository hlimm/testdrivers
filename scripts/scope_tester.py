"""
Adapted from:
https://teledynelecroy.com/doc/using-python-with-activedso-for-remote-communication

Written on Feb 18 2021
@author: Henry Limm
Last Update: 
"""

'''
This file currently is just a script for testing that the connection can be made
'''

import win32com.client as com_cl # import the pywin32 library

addr = 'USB0::0x05FF::0x1023::LCRY4902C18120::INSTR' # Utilities > Utilities Setup

# create an instance of ActiveDSO control 
# NOTE: - Need to have ActiveDSO installed on the controlling (Windows) computer
#       - Also be sure to have NI-VISA installed
#       - The USBTMC connection seems to be a bit finnicky, so try plugging and unplugging, check Device Manager
scope = com_cl.Dispatch('LeCroy.ActiveDSOCtrl.1')
scope.MakeConnection('USBTMC:' + addr)

