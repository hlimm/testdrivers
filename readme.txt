This is intended to be the topmost level of a Python package containing various hardware drivers used during automated tests. I suppose that it could also contain some scripts, but I think that those are better suited to be stored in a separate repository.

At the birth of this package, I'll just have copied in some modules/packages from before my time at Motiv.
Maybe eventually I'll port all the old libraries over and refactor some things (namely CAN_helper.py) so that device modules are a bit more heirarchically organized.

Description of the files I've added:
    hdoActiveDSO.py is a class wrapper for controlling the scope over the
    LeCroy ActiveDSO ActiveX control. It currently only has an __init__ 
    function, after which you control the scope in the following format:
        osc = HDOScope(addr)
        >>> Connected to the scope at: addr
        osc.scope.GetScaledWaveform('C1', maxSamples, 0)
        osc.scope.<any other ActiveDSO method>()
    See the 'ActiveDSO Developer's Guide' pdf provided by LeCroy, as well as 
    the 'Maui Remote Control and Automation Manual.
    I have also made a class method detailing how I'd recommend wrapping the
    ActiveDSO methods for ease of use (no typing osc.scope.<> each time)

    scope_tester.py is a short script demonstrating how to make a connect to 
    the scope using USBTMC. Note that this is done without the HDOscope class.

Henry Limm
Created 2020-Feb-2021
